package main

import (
	"net"
	"post_service/config"
	pb "post_service/genproto/post"
	"post_service/pkg/db"
	"post_service/pkg/logger"
	"post_service/service"
	grpcclient "post_service/service/grpc_client"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "post_service")
	defer logger.Cleanup(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PosgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase),
	)
	connDB, err := db.ConnectToDB(cfg)
	if err != nil {
		log.Fatal("sqlx connection to postgres error", logger.Error(err))
	}
	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("grpc connection to client error", logger.Error(err))
	}
	postService := service.NewPostService(connDB, log, grpcClient)
	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
	s := grpc.NewServer()
	reflection.Register(s)
	pb.RegisterPostServiceServer(s, postService)
	log.Info("main:server running",
		logger.String("port", cfg.RPCPort),
	)
	if err := s.Serve(lis); err != nil {
		log.Fatal("error while listening2: %v", logger.Error(err))
	}
}
