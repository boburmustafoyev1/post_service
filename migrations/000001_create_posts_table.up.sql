CREATE TABLE IF NOT EXISTS posts(
    id SERIAL PRIMARY KEY,
    owner_id INT,
    name TEXT, 
    description TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP
);