CREATE TABLE IF NOT EXISTS medias(
    id SERIAL PRIMARY KEY,
    post_id BIGINT REFERENCES posts(id),
    name TEXT,
    link TEXT,
    type TEXT
);