package repo

import (
	pb "post_service/genproto/post"
)

type PostStorageI interface {
	Create(*pb.PostReq) (*pb.PostResp, error)
	GetPostReview(*pb.ID) (*pb.PostInfo, error)
	GetPostCustomer(OwnerId int64) (*pb.Posts, error)
	UpdatePost(*pb.PostUp) (*pb.PostResp, error)
	DeletePost(*pb.ID) (*pb.Empty, error)
	DeleteCustomerPost(*pb.CustomerId) (*pb.Empty, error)
}
